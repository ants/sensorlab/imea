# Current available shape measurements
[`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) supports the extraction of 2D and 3D shape measurements from images.

## 2D shape measurements
Currently, 53 twodimensional shape measurements are implemented in [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea), as shown below. According to DIN ISO 9276-6 these are structured in macro-, meso- and microdecriptors as well as statistical lengths, as illustrated below.

![macro-, meso- and micromeasurements](https://git.rwth-aachen.de/ants/sensorlab/imea/raw/master/media/macro_meso_micro_measurements.png "macro-, meso- and micromeasurements")

### Macro decriptors (`imea.measure_2d.macro`)
Macrodescriptors represent the overall, external shape of a particle. The following macrodescriptors are currently available in [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea):

  - Perimeter
    - `perimeter`
    - `convex_perimeter`
  - Area
    - `area_projection`
    - `area_filled`
    - `area_convex`
  - Legendre inertia ellipse
    - `major_axis_length`
    - `minor_axis_length`
  - Diameters
    - `diameter_max_inclosing_circle`
    - `diameter_min_enclosing_circle`
    - `diameter_circumscribing_circle`
    - `diameter_inscribing_circle`
    - `diameter_equal_area`
    - `diameter_equal_perimeter`
  - Max dimensions
    - `x_max`
    - `y_max`
  - Minimal bounding box
    - `width_min_bb`
    - `length_min_bb`
  - Geodetic length and thickness
    - `geodeticlength`
    - `thickness`


### Meso decriptors (`imea.measure_2d.meso`)
Mesodescriptors describe details in the particle shape and/or surface structure whose magnitude are not much smaller than the particle proportions. The following mesodescriptors are currently available in [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea):

  - `n_erosions`
  - `n_erosions_complement`


### Micro decriptors (`imea.measure_2d.micro`)
Microdescriptors describe the roughness of particle contours. The following microdescriptors are currently available in [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea):

  - `fractal_dimension_boxcounting_method`
  - `fractal_dimension_perimeter_method`


### Statistical lengths (`imea.measure_2d.statistical_length`)
Statistical lengths are macrodescriptors that are evaluated at different rotation angles of the shape. Based on the resulting distribution of statistical length different metrics like the minimum or maximum value can be obtained. The following statistical length are currently available in [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea):

  - Feret diameter
    - `feret_max`
    - `feret_min`
    - `feret_median`
    - `feret_mean`
    - `feret_mode`
    - `feret_std`
  - Martin diameter
    - `martin_max`
    - `martin_min`
    - `martin_median`
    - `martin_mean`
    - `martin_mode`
    - `martin_std`
  - Nassenstein diameter
    - `nassenstein_max`
    - `nassenstein_min`
    - `nassenstein_median`
    - `nassenstein_mean`
    - `nassenstein_mode`
    - `nassenstein_std`
  - Max chords
    - `maxchords_max`
    - `maxchords_min`
    - `maxchords_median`
    - `maxchords_mean`
    - `maxchords_mode`
    - `maxchords_std`
  - All chords
    - `allchords_max`
    - `allchords_min`
    - `allchords_median`
    - `allchords_mean`
    - `allchords_mode`
    - `allchords_std`

More information on the implementation and references can be find in the [`imea` repository](https://git.rwth-aachen.de/ants/sensorlab/imea).

## 3D shape measurements
For 3D recordings, there are 13 threedimensional shape measurements implemented in [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea):

### Macro decriptors (`imea.measure_2d.macro`)

- Volume and surface area
    - `volume`
    - `volume_convexhull`
    - `surface_area`
- Diameter
    - `diameter_volume_equivalent`
    - `diameter_surfacearea_equivalent`
- Minimal 3D bounding box
    - `width_3d_bb`
    - `length_3d_bb`
    - `height_3d_bb`
- Feret diameter (3D)
    - `feret_3d_max`
    - `feret_3d_min`
- Max dimensions (3D)
    - `x_max_3d`
    - `y_max_3d`
    - `z_max_3d`

*To our best knownledge there are currently no 3D meso and microdecriptors proposed or used in scientific literature or technical norms. If you found any: Please submit an [issue](https://git.rwth-aachen.de/ants/sensorlab/imea/-/issues) with link to the paper where they are proposed and used (see also our guidelines on [contribution](contribution.md)), so we can integrate them into [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) in the future.*

More information on the implementation and references can be find in the [`imea` repository](https://git.rwth-aachen.de/ants/sensorlab/imea).
