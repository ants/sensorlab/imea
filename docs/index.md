# imea: A Python package for extracting 2D and 3D shape measurements from images

[`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) is an open source Python package for the extraction of 2D and 3D shape measurements. The current version of [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) enables the extraction of 53 different 2D shape measurements as well as 13 different 3D shape measurements with a single function call. Both 2D shapes, represented as 2D binary images, as well as 3D shapes, represented as grayscale images where the grayvalue of each pixel represents its height, can be analyzed automatically. Extracted shape measurements are returned as a `pandas` dataframe and by specifying the spatial resolution of inserted images, results are automatically converted into metric units for further quantitative analysis.

![imea logo](https://git.rwth-aachen.de/ants/sensorlab/imea/raw/development/media/imea.png "imea logo")

## Citation
If you use [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea), please cite our [JOSS paper](https://doi.org/10.21105/joss.03091):  

```bibtex
@article{Kroell2021,
  doi = {10.21105/joss.03091},
  url = {https://doi.org/10.21105/joss.03091},   
  year = {2021},
  publisher = {The Open Journal},
  volume = {6},
  number = {60},
  pages = {3091},
  author = {Nils Kroell},
  title = {imea: A Python package for extracting 2D and 3D shape measurements from images},
  journal = {Journal of Open Source Software}
}
```
