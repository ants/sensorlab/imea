# Installation
We recommend using [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) in an [Anaconda](https://www.anaconda.com/) enviroment.

## Install with pip (recommended)
You can install [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) using the [`pip package manager`](https://pypi.org/project/pip/):

```bash
pip install imea
```
## Install from sources
An other option is to clone the [`imea` repository](https://git.rwth-aachen.de/ants/sensorlab/imea) and install it manually:

```bash
git clone https://git.rwth-aachen.de/ants/sensorlab/imea
cd imea
pip install .
```

## Dependencies
[`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) is tested in Python **3.7+**. To use [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) the following packages are required:

  * [`numpy>=1.18`](https://www.numpy.org/)
  * [`scipy>=1.5`](https://www.scipy.org/)
  * [`scikit-image>=0.16`](https://www.scikit-image.org/)
  * [`opencv-python>=4.5`](https://www.opencv.org/)
  * [`pandas>=1.0.5`](https://www.pandas.pydata.org/)
  * [`matplotlib>=3.2`](https://matplotlib.org/) *(only for visualization in demo notebooks)*
  * [`pytest>=5.4`](https://pytest.org) *(only for running tests)*

Requierements are also listed in [`requirements.txt`](./requirements.txt) (for using [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea)) and [`requirements-dev.txt`](./requirements-dev.txt) (for development and running tests).

## Tests

Unit tests are available in the [tests](https://git.rwth-aachen.de/ants/sensorlab/imea/-/tree/master/tests) folder. To execute the tests with [`pytest`](https://pytest.org) you can run the following command:

```bash
  python -m pytest tests
```