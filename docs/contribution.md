# Contribution
If you want to contribute to [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea), feel free to contact Nils Kroell via [nils.kroell@ants.rwth-aachen.de](mailto:nils.kroell@ants.rwth-aachen.de). Moreover, you can do so by reporting bugs and/or suggesting new shape measurements.

## Reporting bugs
If you encounter any issues or inconsistent results using [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea): Please report them via our [issue tracker](https://git.rwth-aachen.de/ants/sensorlab/imea/-/issues), so we can work on them. Please give details on the used version of Python and other dependencies as well as provide exemplary data together with the output of [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) and your expected output, so we can reproduce your error.

## Suggesting new shape measurements
If you miss any 2D or 3D shape measurement feel free to open an issue providing the following details:

 * Scientific paper, where the shape measurement is introduced and defined,
 * evidence why this shape measurement is of scientific relevance (cite at least three scientific papers where the shape measurement is used),
 * suggestions and/or references for implementation (optional).