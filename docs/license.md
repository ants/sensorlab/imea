# License
[`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) is published under the [MIT](https://en.wikipedia.org/wiki/MIT_License)-License.