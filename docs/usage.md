# Usage
You can use [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea) either to extract 2D shape measurements from 2D binary images or to extract 2D as well as 3D shape measurements from grayscale images (heightmaps). Under the folder [**demo**](https://git.rwth-aachen.de/ants/sensorlab/imea/-/tree/master/demo) you can find two Jupyter notebooks that demonstrate the usage of [`imea`](https://git.rwth-aachen.de/ants/sensorlab/imea), as well as several [**example images**](https://git.rwth-aachen.de/ants/sensorlab/imea/-/tree/master/demo/example_imgs).

## 2D measurements
For 2D shape measurements insert a binary image `bw` and the spatial resolution in xy-direction (`spatial_resolution_xy`) in [mm/px] into the function `extract_df_2d`:

```python
df_2d = imea.shape_measurements_2d(bw, spatial_resolution_xy)
```

As a result you get a [`pandas`](https://www.pandas.pydata.org/) dataframe, in which each row represents one particle in the binary image and each column an extracted shape measurement.

**Image calibration and spatial resolution:** If your image is not calibrated (i.e. no "square" pixels) you may use [`skimage.transform.rescale`](https://scikit-image.org/docs/dev/api/skimage.transform.html#skimage.transform.rescale) to calibrate your image. If you want your results just in pixels then set `spatial_resolution_xy=1`.

**Optional parameters:** Optional parameters include the rotation stepsize `dalpha` (in degrees) for determinating statistical length and two boolean variables for experts to return the original distribution of statistical lengths (set `return_statistical_lengths=True`) and all chords (set `return_all_chords=True`).

## 3D measurements
For 3D shape measurements insert a 3D grayscale image (`img_3d`), define a threshold (`threshold_mm` in [mm]) and the spatial resolution of one pixel in x/y-direction (`spatial_resolution_xy` in [mm/Pixel]) and z-direction (`spatial_resolution_z` in [mm/Grayvalue]). Pixels with heights lower then `threshold_mm` are treated as background, the other ones are considered as objects. With the following function call you can extract 2D and 3D shape measurements from a 3D grayscale image (`img_3d`):

```python
df_2d, df_3d = imea.shape_measurements_3d(img_3d, threshold_mm, spatial_resolution_xy, spatial_resolution_z)
```

As a result you get two [`pandas`](https://www.pandas.pydata.org/) dataframes `df_2d` and `df_3d`, in which each row represents one particle in the binary image and each column an extracted shape measurement.

**Image calibration and spatial resolution:** You can define the spatial resolution of your image with the parameters `spatial_resolution_xy` and `spatial_resolution_z`. For calibration and spatial resolution the same recommandations as for the 2D case apply (see above).

**Optional parameters:** Optional parameters include the rotation stepsize `dalpha` for determinating shape measurements like the feret diameter, the minimum number of pixels per object to be considered (`min_object_area`) and the maximum number of objects `n_objects_max` you want to extract from `img_3d`. Set `n_objects_max=-1` if you want to extract all objects, for `n_objects_max > 0` the `n_objects_max` largest objects (determinated by area) are extracted.