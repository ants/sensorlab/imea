numpy>=1.18
scipy>=1.10
scikit-image>=0.16
opencv-python>=4.5
pandas>=1.0.5