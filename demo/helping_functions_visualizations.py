import matplotlib.pyplot as plt
import numpy as np
from scipy import stats


def plot_fractal_dimension_regression(measurements, step_sizes):
    """Plot stepsizes over the calculated perimeters, fit a line and plot it. (Only for visualisation.)

    Parameters
    ----------
    measurements : numpy.ndarray, type int
        Array of analysed step sizes in shape [s0, s1, s2, ...].
    step_sizes : numpy.ndarray, type int
        Array of the corresponding perimeters in form [p0, p1, p2, ...].

    Returns
    -------
    None
    """

    step_sizes_log = np.log2(step_sizes)
    measurements_log = np.log2(measurements)

    slope, intercept, _, _, _ = stats.linregress(step_sizes_log,
                                                 measurements_log)

    plt.plot(step_sizes_log,
             measurements_log,
             'o',
             label='Original data')
    plt.plot(step_sizes_log,
             intercept + slope * step_sizes_log,
             'r',
             label='Fitted line')
    plt.xlabel('log(Stepsize [px])')
    plt.ylabel('log(Measured [px])')
    plt.legend()
    plt.show()

    return


def visualize_walking_path_fractal_dimension_perimeter_method(contour, all_idx_list):
    """Visualize walking path of the perimeter method to determine the fractal dimension of a contour.

    Parameters
    ----------
    contour : numpy.ndarray, type float
        Array of contourpoints describing the shape contour
        in shape [[x0, y0], [x1, y1], [x2, y2], ...]
    all_idx_list : list
        List of the indexes of measurements points used at the different stepsizes in shape
        [[i00, i01, ...], [i10, i11, ...], ...].
        Where each sublist represents the indexes of one walk with a certain step size.

    Returns
    -------
    None
    """
    for idx_list in all_idx_list:
        fig, ax = plt.subplots()
        plt.scatter(contour[:, 0], contour[:, 1], c='grey')
        plt.scatter(contour[idx_list, 0], contour[idx_list, 1], c='red')
        poly = plt.Polygon(contour[idx_list, :],
                           ec="r",
                           fill=False,
                           linewidth=3)
        ax.add_patch(poly)
        ax.axis('equal')
        plt.show()

    return


def transform_feret_idx_to_lines(x_idx_feret, y_max_img, y_min_img=0):
    """Transform the x indexes of the feret calipers into two lines from y_min_img to y_max_img
        for visualisation of feret diameter.

    Parameters
    ----------
    x_idx_feret : numpy.ndarray, type int
        x indexes of feret calipers in shape [idx_x0, idx_x1].
    y_max_img : int
        Max y coordinates of the calculated lines. (Usually width of the image).
    y_min_img : int, optional
        Min y coordinates of the calculated lines, by default 0.

    Returns
    -------
    line_feret_top : numpy.ndarray, type int
        Line showing the top feret caliper in shape [[x0, y0],[x1, y1]].
    line_feret_bottom : numpy.ndarray, type int
        Line showing the bottom feret caliper in shape [[x0, y0],[x1, y1]].
    """
    line_feret_top = np.zeros((2, 2))
    line_feret_top[0, 0] = x_idx_feret[0]
    line_feret_top[0, 1] = 0
    line_feret_top[1, 0] = x_idx_feret[0]
    line_feret_top[1, 1] = y_max_img

    line_feret_bottom = np.zeros((2, 2))
    line_feret_bottom[0, 0] = x_idx_feret[1]
    line_feret_bottom[0, 1] = 0
    line_feret_bottom[1, 0] = x_idx_feret[1]
    line_feret_bottom[1, 1] = y_max_img

    return line_feret_top, line_feret_bottom
