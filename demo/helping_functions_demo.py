import matplotlib.pyplot as plt
import numpy as np
from skimage import color, transform

from imea import measure_2d, tools


def visualize_circles_and_min_bb_on_bw(bw, thickness=3):
    # create image to draw uppon
    img = color.gray2rgb(bw).astype('float')

    fig, axarr = plt.subplots(1, 5, figsize=(17, 5))

    # get measurements from binary image
    contour = measure_2d.utils.find_contour(bw)
    _, _, _, _, _, _, centroid, _, _, _ = measure_2d.utils.skimage_measurements(
        bw)

    # max_inclosing_circle
    max_inclosing_circle_center, max_inclosing_circle_radius =\
        measure_2d.macro.max_inclosing_circle(bw)
    img_max_inclosing_circle = tools.draw.circle_on_img(
        img, max_inclosing_circle_center, max_inclosing_circle_radius, thickness=thickness)
    axarr[0].imshow(img_max_inclosing_circle)
    axarr[0].set_title("Max Inclosing Circle:\nd = {:0.1f} px".format(
        2*max_inclosing_circle_radius))

    # min_enclosing_circle
    min_enclosing_circle_center, min_enclosing_circle_diameter =\
        measure_2d.macro.min_enclosing_circle(contour)
    img_min_enclosing_circle = tools.draw.circle_on_img(img,
                                                        min_enclosing_circle_center,
                                                        min_enclosing_circle_diameter,
                                                        thickness=thickness)
    axarr[1].imshow(img_min_enclosing_circle)
    axarr[1].set_title("Min Enclosing Circle:\nd = {:0.1f} px".format(
        min_enclosing_circle_diameter))

    # circumscribing and inscribing circle
    circumscribing_circle_diameter, inscribing_circle_diameter = \
        measure_2d.macro.circumscribing_and_inscribing_circle(centroid,
                                                        contour)

    img_circumscribing_circle = tools.draw.circle_on_img(img,
                                                         centroid,
                                                         circumscribing_circle_diameter,
                                                         thickness=thickness)
    axarr[2].imshow(img_circumscribing_circle)
    axarr[2].set_title("Circumscribing Circle:\nd = {:0.1f} px".format(
        circumscribing_circle_diameter))

    img_inscribing_circle = tools.draw.circle_on_img(img,
                                                     centroid,
                                                     inscribing_circle_diameter,
                                                     thickness=thickness)
    axarr[3].imshow(img_inscribing_circle)
    axarr[3].set_title("Inscribing Circle:\nd = {:0.1f} px".format(
        inscribing_circle_diameter))

    # min bounding box
    length_min_bb, width_min_bb, _, cornerpoints_min_bb_2d =\
        measure_2d.macro.min_2d_bounding_box(bw)
    img_bb = tools.draw.polyline_on_img(img,
                                        cornerpoints_min_bb_2d,
                                        thickness=thickness)
    axarr[4].imshow(img_bb)
    axarr[4].set_title("Min Bounding Box:\nw = {:0.1f} px, l = {:0.1f} px".format(width_min_bb,
                                                                                  length_min_bb))
    plt.show()

    return


def visualize_statistical_length_on_bw(bw, rotation_angle=0, thickness=3):
    bw = transform.rotate(bw, rotation_angle, resize=True) > 0.5
    fig, axarr = plt.subplots(1, 4, figsize=(17, 5))
    img = color.gray2rgb(bw).astype('float')

    # Feret diameter
    feret_diameter, x_idx = measure_2d.statistical_length.feret(bw)
    feret_line1 = np.array([[x_idx[0], 0], [x_idx[0], bw.shape[1]]])
    feret_line2 = np.array([[x_idx[1], 0], [x_idx[1], bw.shape[1]]])
    img_feret = tools.draw.polyline_on_img(img,
                                           feret_line1,
                                           thickness=thickness)
    img_feret = tools.draw.polyline_on_img(img_feret,
                                           feret_line2,
                                           thickness=thickness)
    axarr[0].imshow(img_feret)
    axarr[0].set_title("Feret diameter:\n{:0.1f} px".format(feret_diameter))

    # martin diameter
    martin_diameter, idx_martin = measure_2d.statistical_length.martin(bw)
    img_martin = tools.draw.polyline_on_img(img,
                                            idx_martin,
                                            thickness=thickness)
    axarr[1].imshow(img_martin)
    axarr[1].set_title(
        "Martin diameter:\n{:0.1f} px".format(martin_diameter))

    # nassenstein diameter
    nassenstein_diameter, idx_nassenstein = measure_2d.statistical_length.nassenstein(bw)
    img_nassenstein = tools.draw.polyline_on_img(img,
                                                 idx_nassenstein,
                                                 thickness=thickness)
    axarr[2].imshow(img_nassenstein)
    axarr[2].set_title(
        "Nassenstein diameter:\n{:0.1f} px".format(nassenstein_diameter))

    # max chord
    max_chord, line_max_chord = measure_2d.statistical_length.find_max_chord(bw)
    img_max_chord = tools.draw.polyline_on_img(img,
                                               line_max_chord,
                                               thickness=thickness)
    axarr[3].imshow(img_max_chord)
    axarr[3].set_title("Max chord:\n{:0.1f} px".format(max_chord))
    plt.show()

    return


def plot_statistical_lenghts_distributions(bw, dalpha=9):
    feret, martin, nassenstein, max_chords, _,\
        angles = measure_2d.statistical_length.compute_statistical_lengths(bw,
                                                                           dalpha)
    n_bins = 10
    fig, axarr = plt.subplots(2, 4, figsize=(17, 7))

    dists = [feret, martin, nassenstein, max_chords]
    names = ["Feret diameter", "Martin diameter",
             "Nassenstein diameter", "Max chord"]

    for i, (dist, name) in enumerate(zip(dists, names)):
        axarr[0, i].scatter(angles, dist,
                            facecolors='none', edgecolors='C0')
        axarr[0, i].set_xlim(0, 180)
        axarr[0, i].set_xlabel("Angle [°]")
        axarr[0, i].set_ylabel("Statistical length [px]")
        axarr[0, i].set_title(name, fontweight="bold")

        _ = axarr[1, i].hist(dist, bins=n_bins)
        axarr[1, i].set_xlabel("Statistical length [px]")
        axarr[1, i].set_ylabel("Frequency [-]")

    plt.tight_layout()
    plt.show()

    return
