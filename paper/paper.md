---
title: 'imea: A Python package for extracting 2D and 3D shape measurements from images'
tags:
  - Python
  - image processing
  - shape measurements
  - binary images
  - region props

authors:
  - name: Nils Kroell
    orcid: 0000-0002-6016-5144
    affiliation: 1
affiliations:
 - name: Department of Anthropogenic Material Cycles, RWTH Aachen University, Aachen, Germany
   index: 1
date: 26 February 2021
bibliography: paper.bib

---

# Summary

Quantitative measurement of 2D and 3D shapes from images is used in many research fields, for example, chemistry [@Lau2013], mineral engineering [@Andersson2012], medicine [@Nguyen2005], biology [@Smith1996], and environmental engineering [@Kandlbauer2021; @Weissenbach2021]. In the past, a variety of shape measurements have been proposed in the scientific literature and as technical norms [@Pahl1973a; @Pahl1973b; @Pahl1973c; @Pabst2007; @Steuer2010; @DIN9276-6].

``imea`` is an open source Python package for extracting 2D and 3D shape measurements from images. The current version of ``imea`` enables the extraction of 53 different 2D shape measurements, covering *macrodescriptors* such as  minimal bounding boxes [@Steuer2010], *mesodescriptors* such as the numbers of erosion to erase a binary image [@DIN9276-6], *microdescriptors* such as the fractal dimension [@DIN9276-6], as well as *statistical lengths* like Feret, Martin or Nassenstein diameters [@Pahl1973a], as shown by the exemplary selection in \autoref{fig:2d_shape_measurements}. Furthermore, 13 different 3D shape measurements ranging from volume [@Pahl1973a] and minimal 3D bounding boxes [@Steuer2010] to 3D Feret diameters and maximum dimensions [@Steuer2010] can be extracted.

Both 2D shapes, represented as 2D binary images, as well as 3D shapes, represented as grayscale images where the grayvalue of each pixel represents its height, can be analyzed automatically with a single function call. Extracted shape measurements are returned as a *pandas* dataframe [@McKinney2010], and by specifying the spatial resolution of inserted images, results are automatically converted into metric units for further quantitative analysis.

# Statement of need

Only a minority of 2D and 3D shape measurements proposed in the scientific literature and as technical norms are available in existing open source packages for image processing, such as *scikit-image* [@vanderWalt2014] and *OpenCV* [@Itseez2015]. In the past, unavailable shape measurements had to be implemented manually by individual researchers or could not be used at all. Moreover, the utilization of different existing packages for shape measurement extraction requires switching between different coordinate systems and data formats. Both cases create unnecessary “reinventing the wheel” and may induce potential calculation errors.

``imea`` solves this problem by simplifying the extraction of 2D and 3D shape measurements from images into a single function call. Researchers can focus on the analysis and utilization of extracted shape measurements, while the shape measurement extraction is handled by ``imea``. A computationally efficient implementation of underlying algorithms makes even complicated shape measurements available to a wide variety of researchers from different fields.

![Exemplary selection of 2-dimensional shape measurements available in ``imea``.\label{fig:2d_shape_measurements}](fig_imea_2D_shapemeasurements.png)

# Acknowledgements

The development of ``imea`` was funded by the German Federal Ministry for Economic Affairs and Energy within the “Central Innovation Programme for small and medium-sized enterprises (SMEs)” under the project PROBE (grant no. 16KN080621) and the Austrian Research Promotion Agency within the programme “Production of the Future” under the project EsKorte (grant no. 877341).

# References
